const express = require("express");
const fs = require("fs");
const marked = require("marked");
const router = express.Router();
const filePath = path.join(routes, "example.md");

router.get("/", (req, res) => {
  // Read the Markdown file
  fs.readFile(filePath, "utf-8", (err, data) => {
    if (err) {
      console.error(err);
      return res.status(500).send("Error reading Markdown file");
    }

    // Convert Markdown to HTML
    console.log("not error");
    const htmlContent = marked(data);

    // Send the HTML response
    res.send(htmlContent);
  });
});

module.exports = router;
