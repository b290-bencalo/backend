const Course = require("../models/Course");

// Create a new course
/*
    Steps:
    1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
    2. Save the new User to the database
*/

module.exports.addCourse = (reqBody) => {
  // MINI ACTIVITY
  // Create/Instantiate a new object for new course
  let newCourse = new Course({
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  });
  return newCourse
    .save()
    .then((course) => true)
    .catch((err) => {
      console.log(err);
      return false;
    });
};

module.exports.getAllCourses = () => {
  return Course.find({})
    .then((course) => course)
    .catch((err) => {
      console.log(err);
      return false;
    });
};

// Retrieve all ACTIVE courses
/*
    Steps:
    1. Retrieve all the courses from the database with the property of "isActive" to true
*/
module.exports.getAllActiveCourses = () => {
  return Course.find({ isActive: true })
    .then((course) => course)
    .catch((err) => {
      console.log(err);
      return false;
    });
};
// Retrieving a specific course
/*
    Steps:
    1. Retrieve the course that matches the course ID provided from the URL
*/
module.exports.getCourse = (reqParams) => {
  return Course.findById(reqParams.courseId)
    .then((result) => result)
    .catch((err) => {
      console.log(err);
      return false;
    });
};
// Update a course
/*
    Steps:
    1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
    2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/
// Information to update a course will be coming from both the URL parameters and the request body
module.exports.updateCourse = (reqParams, reqBody) => {
  let updatedCourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  };
  // Syntax
  // findByIdAndUpdate(document ID, updatesToBeApplied)
  return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
    .then((course) => true)
    .catch((err) => {
      console.log(err);
      return false;
    });
};

module.exports.archiveCourse = (reqParams, reqBody) => {
  return Course.findByIdAndUpdate(reqParams.courseId, {
    isActive: reqBody.isActive,
  })
    .then((course) => true)
    .catch((err) => {
      console.log(err);
      return false;
    });
};

module.exports.addCourses = (coursesArray) => {
  // MINI ACTIVITY
  // Create/Instantiate new objects for each course in the array
  const newCourses = coursesArray.map((course) => {
    return new Course({
      name: course.name,
      description: course.description,
      price: course.price,
    });
  });
  // Use the `insertMany` function to save all the new courses at once
  return Course.insertMany(newCourses)
    .then((course) => {
      console.log(course);
      return course;
    })
    .catch((err) => {
      console.log(err);
      return false;
    });
};

module.exports.createOrder = async (data) => {
  try {
    const product = await Product.findById(data.productId);
    if (product) {
      if (product.quantity >= data.quantity && product.isActive) {
        const newOrder = new Order({
          userId: data.userId,
          firstName: data.firstName,
          products: [
            {
              productId: data.productId,
              name: product.name,
              quantity: data.quantity,
            },
          ],
          totalAmount: data.quantity * product.price,
        });
        product.quantity -= data.quantity;
        product.isActive = product.quantity >= 1;

        await product.save();

        const savedOrder = await newOrder.save();
        console.log(savedOrder);
        return savedOrder;
      } else {
        console.log("Quantity is not enough or product is not active");
        return false;
      }
    } else {
      console.log("Product does not exist");
      return false;
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};