/* 
    A database is an organized collection of information or data.

    Data is different from information. Data is raw and does not carry any specific meaning.
    Information on the other hand is a group of organized data that contains logical meaning.

    Databases typically refer to information stored in a computer system but it can also refer to physical databases
    One such example is a library which is a database of books

    Database is manage by what is called a database management system.

    What is database management system?
        DBMS is a system specifically designed to manage the storage retrieval and modification of data in a database

        DBMS of a physical library is the Dewey Decimal System

    Database systems allow four types of operations when it comes to handling data, namely create(insert), read(select), update and delete. Collectively it is called the CRUD Operations

    Relational database is a type of database where data is stored as a set of tables with rows and columns (pretty much like a table printed on a piece of paper). Ex. SQL

    For unstructured data (data that cannot fit into a strict tabular format). NoSQL databases are commonly used.

    What is SQL?
    SQL stands for structured query language. It is the language used typically relational DBMS to store, retrieve and modify data.

    SELECT id, first_name, last_name FROM students WHERE batch_number = 1;

    Information represented in SQL are done in the form of tables having columns and rows that provide information about the data:

    SQL databases require tables and information provided in it's columns to be defined before they are created and then used to store information

    This ensures that information stored in SQL databases to be of the same data structure preventing any errors of storing data that are incomplete.

    Because of SQL databases require tables and columns to be defined before creation and use, NoSQL databases are becoming more popular due to their flexibility of being able to change the data structure of information in databases on the fly.

    Developers can omit the process of recreating tables and backing up data and re importing them as needed on certain occasions.

    What is NoSQL?

    NoSQL means Not only SQL. Was conceptualized when capturing complex, unstructured data became more difficult. One of the popular NoSQL databases is MongoDB.

    What is MongoDB?

    MongoDB is an open-source database and the leading NoSQL database. Its language is highly expressive and generally friendly to those already familiar with the JSON structure.

    Mongo in MongoDB is a part of the word humongous which then means huge or enormous.

    What does data stored in MongoDB look like?

    Since MongoDB is a database that uses key-value pairs, our data looks like this >> Same as JSON structure
    
    The main advantage of this data structure is that it is stored in MongoDB using the JSON Format.

    A developer does not need to keep in mind the difference between writing code for the program itself and data access in a DBMS

    Also, in MongoDB, some terms in relational databases will be changed:
        from tables to collections;
        from rows to documents for rows; and
        from columns to fields
    

*/
