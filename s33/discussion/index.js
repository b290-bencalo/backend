/*
API - Application Programming Interface - part of server responsible for receiving requests and sending responses
    
What is it for?
    - HIdes the server beneath an interface layer
        - HIdded complexity makes apps easier to use
        - Sets the rules of interaction between front and back ends of an application, improving security
    
What is REST - REpresentational State Transfer
 - An architectural style for providing standards for communication between computer systems

 What problem does it solve?
    - The need to separate user interface concerns of the client from data storage to another server
How does it solve the problem?
    - Statelessness - Server does not need to know client state and vice versa
                    - Every client request has all the information it needs to be processed by the API
                    - Made possible via resources and HTTP methods

    - Standardized Communication - Enable a decoupled server to understand, process and respond to client requests without knowing client state
                                - Implemented via resources represented as Uniform Resource Identifier (URI) endpoints and HTTP methods - Resources re plural by convention.
                                - Ex. /api/photos (Never api/photo)
Anatomy of a Client Request
    - Operation to be performed dictated by HTTP methods
    REST API MEthods
        HTTP Methods
            - GET, POST, PUT, DELETE
        CRUD Operations
            - INSERT, UPDATE, SELECT, DELETE

    Anatomy of a Client Request
        - Method
        - A path to the resource to be operated on API request
        - A header containing additional request information Content-Type
        - A request body containing data (optional)

The resource is jokes
/programming denotes the resource category
/random determines the specific joke that will be sent via a response
Hitting enter on your browser sends a GET request to the API endpoint
Given a URI endpoint and a method, the API was able to respond


        
*/

