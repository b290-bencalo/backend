// s30 MongoDB Aggregation

/*
    - Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
    - Compared to doing CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a frontend application.
*/

// Using the aggregate method
/*
    - The "$match" is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process.
    - Syntax
        - { $match: { field: value } }
    - The "$group" is used to group elements together and field-value pairs using the data from the grouped elements
    - Syntax
        - { $group: { _id: "value", fieldResult: "valueResult" } }
    - Using both "$match" and "$group" along with aggregation will find for products that are on sale and will group the total amount of stocks for all suppliers found.
    - Syntax
        - db.collectionName.aggregate([
            { $match: { fieldA, valueA } },
            { $group: { _id: "$fieldB" }, { result: { operation } } }
        ])
    - The "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
    - The "$sum" operator will total the values of all "stock" fields in the returned documents that are found using the "$match" criteria.
*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
]);

// Field projections with aggregation

/*
    - The "$project" can be used when aggregating data to include/exclude fields from the returned results
    - Syntax
        - { $project : { field: 1/0 } }
*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $project: { _id: 0 } },
]);

// Sorting aggregated results

/*
    - The "$sort" can be used to change the order of aggregated results
    - Providing a value of -1 will sort the aggregated results in a reverse order
    - Syntax
        - { $sort { field: 1/-1 } }
*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
  { $sort: { total: -1 } },
]);

// Aggregating results based on array fields
/*
    - The "$unwind" deconstructs an array field from a collection/field with an array value to output a result for each element.
    - The syntax below will return results creating separate documents for each of the countries provided per the "origin" field
    - Syntax
        - { $unwind: field }
*/

db.fruits.aggregate([{ $unwind: "$origin" }]);

db.fruits.aggregate([
  { $unwind: "$origin" },
  { $group: { _id: "$origin", kinds: { $sum: 1 } } },
]);

// [Section] Guidelines on Schema Design
/*
    - Schema design/data modelling is an important feature when creating databases.
    - MongoDB documents can be categorized into normalized and de-normalized/embedded data.
    - Normalized data refers to a data structure where documents are referred to each other using their ids for related pieces of information.
    - De-normalized data/embedded data refers to a data structure where related pieces of information is added to a document as an embedded object.
    - Both data structures are common practice but each of them have their pros and their cons.
    - Normalized data makes it easier to read information because separate documents can be retrieved but in terms of querying results, it performs slower compared to embedded data due to having to retrieve multiple documents at the same time.
    - This approach is recommended for data structures where pieces of information are commonly operated on/changed.
    - De-normalized data/embedded data makes it easier to query documents and has a faster performance because only one query needs to be done in order to retrieve documents. However, if the data structure becomes too complex it makes it more difficult to manipulate and access information.
    - This approach is recommended for data structures where pieces of information are commonly retrieved and rarely operated on/changed.
*/

// One-to-one Relationship

db.owners.insertOne({
  name: "John Smith",
  contact: "09123456789",
});

db.suppliers.insertOne({
  name: "ABC Fruits",
  contact: "1234567890",
  owner_id: "6474428d610cbf49632ff8ba",
});

db.suppliers.updateOne(
  { name: "ABC Fruits" },
  {
    $set: {
      owner_id: ObjectId("6474428d610cbf49632ff8ba"),
    },
  }
);

// One to few relationship

db.suppliers.insertOne({
  name: "DEF Fruits",
  contact: "1234567890",
  address: [
    { street: "123 San Jose St.", city: "Manila" },
    { street: "367 Gil Puyat", city: "Makati" },
  ],
});

// One to many Relationship

var supplier = ObjectId("6474481e610cbf49632ff8bd");
var branch1 = ObjectId("647448e1610cbf49632ff8be");
var branch2 = ObjectId("6474498c610cbf49632ff8bf");

db.suppliers.insertOne({
  name: "GHI Fruits",
  contact: "1234567890",
  branches: [
    { branch_id: ObjectId("647448e1610cbf49632ff8be") },
    { branch_id: ObjectId("6474498c610cbf49632ff8bf") },
  ],
});

db.branches.insertOne({
  name: "BF Homes",
  address: "123 Arcardio Santos St.",
  city: "Parañaque",
  supper_id: ObjectId("6474481e610cbf49632ff8bd"),
});

db.branches.insertOne({
  name: "Rizal Homes",
  address: "123 San Jose St.",
  city: "Manila",
  supper_id: ObjectId("6474481e610cbf49632ff8bd"),
});

db.suppliers.updateOne(
  { name: "GHI Fruits" },
  {
    $set: {
      branches: [
        { branch_id: ObjectId("647448e1610cbf49632ff8be") },
        { branch_id: ObjectId("6474498c610cbf49632ff8bf") },
      ],
    },
  }
);
